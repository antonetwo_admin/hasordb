/*
 * Copyright 2002-2005 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.hasor.db.lambda;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

/**
 * lambda Update 执行器
 * @version : 2020-10-31
 * @author 赵永春 (zyc@hasor.net)
 */
public interface UpdateExecute<T> extends BoundSqlBuilder {
    /** 生成 select count() 查询语句并查询总数。*/
    public int doUpdate() throws SQLException;

    /** 允许空 Where条件（注意：空 Where 条件会导致更新整个数据库） */
    public UpdateExecute<T> allowEmptyWhere();

    /** 更新数据，map key 为列名 */
    public UpdateExecute<T> updateByColumn(Map<String, Object> newValue);

    /** 更新数据，map key 为列名 */
    public UpdateExecute<T> updateByColumn(Collection<String> setColumns, T newValue);

    /** 所有属性都作为 set 的值 */
    public UpdateExecute<T> updateTo(T newValue);

    /** sample 中不为空的属性作为 set 的值 */
    public UpdateExecute<T> updateBySample(T sample);
}